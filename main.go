package main

import (
	"AWSapp/handlers"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

func main() {
	e := echo.New()
	e.Use(middleware.BodyDump(func(c echo.Context, reqBody, resBody []byte) {}))
	e.Static("/", "html")
	e.GET("/", handlers.GetIndexHandler)
	e.POST("/create-instances", handlers.CreateInstancesHandler)
	e.Start(":8082")
}
