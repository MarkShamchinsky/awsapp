package handlers

import (
	"context"
	"encoding/base64"
	"fmt"
	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/ec2"
	"github.com/aws/aws-sdk-go-v2/service/ec2/types"
	"github.com/labstack/echo"
	"io"
	"net/http"
	"path/filepath"
	"strconv"
	"strings"
)

func GetIndexHandler(c echo.Context) error {
	return c.File("html/index.html")
}

func CreateInstancesHandler(c echo.Context) error {

	// Получить параметры из формы.
	instanceCountStr := c.FormValue("instanceCount")
	instanceType := c.FormValue("instanceType")
	pemKeyFile, err := c.FormFile("pemKeyFile")

	if err != nil {
		fmt.Println("Ошибка получения файла ключа:", err)
	}

	amiID := "ami-0c38efb4f5f15205f"
	// Преобразовали строчное значение в число
	instanceCount, err := strconv.Atoi(instanceCountStr)
	if err != nil {
		fmt.Println("Ошибка преобразования instanceCount:", err)
	}

	// Получение имя файла ключа без расширения
	keyFileName := filepath.Base(pemKeyFile.Filename)
	keyName := strings.TrimSuffix(keyFileName, filepath.Ext(keyFileName))

	// Создать новую конфигурацию для SDK AWS.
	cfg, err := config.LoadDefaultConfig(context.TODO(), config.WithRegion("us-east-1"))
	if err != nil {
		fmt.Println("Ошибка загрузки конфигурации AWS:", err)
	}

	// Создать клиент EC2.
	clientEC2 := ec2.NewFromConfig(cfg)

	// Создать параметры для новых EC2 инстансов.
	runInput := &ec2.RunInstancesInput{
		ImageId:      &amiID,
		InstanceType: types.InstanceType(instanceType),
		MinCount:     aws.Int32(int32(instanceCount)),
		MaxCount:     aws.Int32(int32(instanceCount)),
	}

	// Загрузка содержимого ключа .pem

	pemKeyFileContent, err := pemKeyFile.Open()
	if err != nil {
		fmt.Println("Ошибка чтения файла ключа:", err)
	}

	// Чтение содержимого ключа .pem

	pemKeyFileBytes, err := io.ReadAll(pemKeyFileContent)
	if err != nil {
		fmt.Println("Ошибка чтения содержимого файла ключа:", err)
	}

	// Добавление параметров ключа

	runInput.KeyName = aws.String(keyName)
	runInput.UserData = aws.String(base64.StdEncoding.EncodeToString(pemKeyFileBytes))

	// Запустите новые инстансы.
	runResult, err := clientEC2.RunInstances(context.TODO(), runInput)
	if err != nil {
		fmt.Println("Ошибка создания инстансов:", err)
	}

	// Получить список инстансов
	var instanceIDs []string

	for _, instance := range runResult.Instances {
		instanceIDs = append(instanceIDs, *instance.InstanceId)
	}
	instanceIDsStr := strings.Join(instanceIDs, ", ")

	return c.String(http.StatusOK, "Инстансы успешно созданы! ID инстансов: "+instanceIDsStr)
}
